# Changelog

## 1.0.1

### Fixed
- Fixed missing _self_ in class method


## 1.0.0

### Breaking changes
- Support for Python 3.6 has been dropped.
- Exceptions are now not caught by this package - it's your job now (#23)
- `Sender.send_bulk` method signature changed. Use a list of `SenderData` objects instead of a request.
- `AgentActive.send_collected_data`, `Sender.send_value`, `Sender.send_file` and `Sender.send_bulk`
  Now return an instance of `ServerInfo` instead of a dict.
- `PING` is no longer a field in `ServerRequestTypeEnum`, also there is an alias now.

### Updated
- Pipelines now use newer Python versions

### Fixed
- Some docstrings contained wrong information.
