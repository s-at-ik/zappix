import unittest
from zappix.protocol import ServerRequest, ModelEncoder
import json
from ast import literal_eval


class TestServerRequest(unittest.TestCase):
    def test_queue_overview(self):
        req = ServerRequest('queue.get', 'overview', 'nosid')
        tested = json.dumps(req, cls=ModelEncoder)
        expected = {"request": "queue.get", "type": "overview", "sid": "nosid"}
        self.assertEqual(literal_eval(tested), expected)

    def test_queue_overview_by_proxy(self):
        req = ServerRequest('queue.get', 'overview by proxy', 'nosid')
        tested = json.dumps(req, cls=ModelEncoder)
        expected = {"request": "queue.get", "type": "overview by proxy", "sid": "nosid"}
        self.assertEqual(literal_eval(tested), expected)

    def test_queue_details(self):
        req = ServerRequest('queue.get', 'details', 'nosid', limit=100)
        tested = json.dumps(req, cls=ModelEncoder)
        expected = {"request": "queue.get", "type": "details", "limit": "100", "sid": "nosid"}
        self.assertEqual(literal_eval(tested), expected)
